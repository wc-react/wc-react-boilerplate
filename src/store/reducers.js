
export const user = (state = {}, action = { type: null}) => {
  switch(action.type){
  case 'ACTION':
    return { ...action.account };
  default:
    return state;
  }
};
