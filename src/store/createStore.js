import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web 
import { user } from './reducers';
// import createSagaMiddleware from 'redux-saga'
// import saga from './sagas'


//create saga middle ware
//const sagaMiddleware = createSagaMiddleware();

const combinedReducers = combineReducers({
  user
});

//Configuration of redux-persist. NOTE: We can choose where we want to save the redux states (e.g. Session Memory, Local Storage)
//for now, we used LocalStorage
const persistConfig = {
  key: 'root',
  storage,
};

//persist the reducer. NOTE: We can select which reducers we want to persist in local storage in the future
const persistedReducer = persistReducer(persistConfig, combinedReducers);

const clientLogger = store => next => action => {
  if (action.type) {
    const result = next(action);
    console.groupCollapsed('dispatching', action.type);
    console.log('prev state', store.getState());
    console.log('action', action);
    console.log('next state', store.getState());
    console.groupEnd();
    return result;
  } else {
    return next(action);
  }
};

const serverLogger = store => next => action => {
  console.log('\n  dispatching server action\n');
  console.log(action);
  console.log('\n');
  return next(action);
};

const middleware = server => [
  (server) ? serverLogger : clientLogger,
  thunk
  //,sagaMiddleware
];

// const storeFactory = (server = false) =>
//   applyMiddleware(...middleware(server));persistStore((createStore)(persistedReducer));


export default (server = false) => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  //Create the store using the persisted reducers and middlewares
  let store = createStore(persistedReducer, composeEnhancers(applyMiddleware(...middleware(server))));
  //Persist the store
  let persistor = persistStore(store);
  //run the saga
  // sagaMiddleware.run(mySaga)
  return { store, persistor };
};


// export default storeFactory;
