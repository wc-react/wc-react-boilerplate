
//get environment variables, if null, use the default host for json-server
const host = process.env.PROD_SERVER || 'http://localhost:3000';

export const USERS = host + '/users/';
export const TEST = host + '/tests/';
