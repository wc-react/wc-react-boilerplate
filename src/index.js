import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createStore from './store/createStore';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {
  HomeRoute,
  NotFound
} from './routes';
import { PersistGate } from 'redux-persist/integration/react';
import './styles/antd-restyle.less';

const { store, persistor } = createStore();

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={ HomeRoute }/>
          <Route path="/home" exact component={ HomeRoute }/>
          <Route component={  NotFound }/>
        </Switch>
      </BrowserRouter>
    </PersistGate>
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
