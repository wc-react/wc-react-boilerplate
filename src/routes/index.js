import HomeRoute from './Home';
import NotFound from './NotFound';

export {
  HomeRoute,
  NotFound
};
