import React from 'react';
import ReactDOM from 'react-dom';
import HomeRoute from '../../../routes/Home';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HomeRoute />, div);
  ReactDOM.unmountComponentAtNode(div);
});
