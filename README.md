White Cloak `create-react-app` Boilerplate (`npm run eject`)
===

+ This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
+ This project also uses **Fractal Project Structure**

```bash
├── components                                  // Reusable components
│   └── Button
│       ├── SubComponent                        // Sub component
│       └── index.js
├── containers                                  // Reusable containers
│   ├── README.md
│   └── index.js
├── db                                          // Database
│   └── db.json
├── helpers                                     // Generics functions commonly used by different components
│   └── validations                             // Reusable validation functions
│   └── constantMessages.js                     // Success, Error, Information and other messages
├── index.js
├── lay_outs                                    // Components that dictate major file structure
│   └── index.js
├── routes                                      // Route definitions
│   ├── Home                                    // Fractal route
│   │   ├── assets                              // Assets(images, svgs, ...etc)
│   │   │   ├── some_assets.svg
│   │   │   └── styles
│   │   │       └── style.css
│   │   ├── components                          // Presentational React Components
│   │   │   └── index.js
│   │   ├── container                           // Connects components to actions and stores
│   │   │   └── index.js
│   │   ├── index.js
│   │   ├── modules                             // Collections of actions, reducers, constants
│   │   │   ├── constants
│   │   │   ├── actions
│   │   │   │   ├── action_creators.js
│   │   │   │   └── index.js
│   │   │   └── reducers
│   │   │       └── index.js
│   │   └── routes                              // Fractal sub routes
│   ├── index.js
│   └── Root.js                                 // Wrapper component
├── serviceWorker.js
├── services                                    // Functions used for APi calls
├── static                                      // Static files/assets
│   ├── logo.svg
│   ├── somefile.svg
│   └── static.json
├── store                                       // Redux specific
│   ├── createStore.js
│   └── reducers.js
├── styles                                      // Application wide style
│   ├── index.css
│   └── styles.css
└── tests                                       // Unit tests
    └── routes
        └── Home
            └── Home.test.js
```

+++ This project also uses redux-persist (https://github.com/rt2zz/redux-persist). This persists the redux states in the browser's localStorage. To check the persisted redux states, go to browser's console (F12)-> application -> local storage.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm run mock:server`
Install `json-server` package globally using: `npm install json-server -g`<br>
Creates a fake api server (`json-server`).

### `npm run eslint`
Executes eslint (`Javascript linter`)

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
